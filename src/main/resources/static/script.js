'use strict';


async function getTasks() {
    const response = await fetch('http://localhost:5050/tasks');
    if (response.ok) {
        let json = await response.json();
        json.forEach(elem => {
            !document.getElementById(elem.id)&&addNewTask(createTaskElement(elem));
        })
        document.getElementById("task").value='';
        document.getElementById("task").focus();
    } else {
        alert("Ошибка HTTP: " + response.status);
    }
}

function addNewTask(x) {
    let y = document.getElementById('list');
    y.insertBefore(x, y.childNodes[1]);
}

function createTaskElement(y) {
    let li = document.createElement('li');
    const html = `
     ${y.taskName}
    `
    li.innerHTML += html;
    li.setAttribute("id", y.id)
    return li;
}

$(document).ready(function() {
    $('#btn').click(getTasks);
});

const addTaskForm = document.getElementById('form');
addTaskForm.addEventListener('submit', addTask);

const url = '/task/addTask';

async function addTask(e) {
    e.preventDefault();
    let h = new Headers();
    h.append('Accept', 'application/json');
    let fd = new FormData();
    let task = document.getElementById('task').value;
    if (task.length < 2) {
        alert("You should enter 2 symbols min");
        document.getElementById("task").value = '';
        document.getElementById("task").focus();
        return;
    } else {
        fd.append('task', task);
        let req = new Request(url, {
            method: 'POST',
            headers: h,
            body: fd
        });

        fetch(req).then(()=> getTasks())
            .catch((err) => {
                console.log('ERROR:', err.message);
            });
    }
}

let list = document.getElementById('list');
list.addEventListener('dblclick', function(ev) {
    if (ev.target.tagName === 'LI') {
        ev.target.classList.toggle('checked');
    }
}, false);



