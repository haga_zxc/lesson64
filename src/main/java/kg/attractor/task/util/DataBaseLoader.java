package kg.attractor.task.util;

import kg.attractor.task.repository.TaskRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class DataBaseLoader {


    @Bean
    CommandLineRunner initDatabase(TaskRepository taskRepository) {
        return (args) -> {
            taskRepository.deleteAll();
        };
    }
}