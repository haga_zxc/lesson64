package kg.attractor.task.controller;


import kg.attractor.task.model.Task;
import kg.attractor.task.repository.TaskRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/tasks")
public class RController {
    private final TaskRepository taskRepository;

    public RController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }
    @GetMapping
    public List<Task> getTasks(){
        return taskRepository.findAll();
    }
    @GetMapping("/{id}")
    public List<Task> getTasksById(@PathVariable("id") String id){
        return  taskRepository.findAllById(id);
    }
}
