package kg.attractor.task.controller;

import kg.attractor.task.model.Task;
import kg.attractor.task.repository.TaskRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/task")
public class TaskController {
    private final TaskRepository taskRepository;

    public TaskController(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @GetMapping
    public String getPage(){
        return "index";
    }
    @PostMapping(path = "/addTask", produces = MediaType.APPLICATION_JSON_VALUE)
    public String saveTask(@RequestParam("task") String task){
        var newTask = new Task(task);
        taskRepository.save(newTask);
        return "redirect:/task";
    }

}
