package kg.attractor.task.repository;


import kg.attractor.task.model.Task;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.List;


public interface TaskRepository extends CrudRepository<Task,String> {
    List<Task> findAll();
    List<Task> findAllById(String id);

}
